$(function() {
  /* заменим стандартные селектбоксы - плагином chosen */
  $('select.chosen').chosen({
    disable_search: true,
    disable_search_threshold: 10
  });

  /* обработчик форм с атрибутом data-remote="true" */
  $(document).on('submit', 'form[data-remote]', function(e) {
    e.preventDefault();
    form_errors_clear(this);
    $(this).ajaxSubmit({
      dataType: 'script'
    });
  });

  /* обработчик ссылок с атрибутом data-remote="true" */
  $(document).on('click', 'a[data-remote]', function(e) {
    e.preventDefault();

    var method = $(this).data('method') || 'get';
    var url = $(this).attr('href');
    var post_data = {};

    if (method == 'post') {
      for (i in $(this).data()) {
        if (i.indexOf('post') === 0) {
          post_data[i.replace(/^post/, '').toLowerCase()] = $(this).data(i);
        }
      }
    }

    $.ajax({
      url : url,
      cache : false,
      dataType : 'script',
      type : method,
      data: post_data
    });
  });

  $('.js_change-socials').on('click', function(event) {
    event.preventDefault();
    $('.js_social-info').addClass('hidden');
    $('.js_socials-buttons').removeClass('hidden');
  })


  $('.js-confidantial_check').on('change', function () {
    var submitBtn = $(this).closest('#feedback_form').find('button');
    var isCheckboxActive = $(this).is(':checked');

    submitBtn
      .toggleClass('disableble', !isCheckboxActive)
      .attr('disabled', !isCheckboxActive);
  });


  /*
    * Добавляем класс loading к кнопкам с data-remote="true" и к обычным кнопкам сабмита формы.
  */
  $('a[data-remote="true"], button[type="submit"], input[type="submit"]').on('click', function(){
    $(this).addClass('loading')
  })

  $( document ).ajaxComplete(function( event,request, settings ) {
    setTimeout(function(){
      $('a.loading, button.loading, input.loading').removeClass('loading');
    }, 1000);
  });

  $( document ).ajaxError(function(event, jqXHR, ajaxSettings, thrownError) {
    //alert('ajax error');
    var msg = typeof(thrownError.message) !== 'undefined' ? thrownError.message : 'no msg';
    ga('send', 'event', 'Ajax Error', document.location.href, ajaxSettings.url + ' || ' + msg); // send alert to ga
    $('a.loading, button.loading, input.loading').removeClass('loading');
  });
});

/*
 * выводит нотайс в шапке
 * text - строка - текст нотайса
 * type - может не быть. или может быть notice, error, success
 * duration - время, через которое нужно скрыть
 */

function show_global_notice(text, type, duration){
  var notice = $('<div class="notice '+type+' "><div class="notice__text">' + text + '</div><span class="notice__close" id="notice_close" title="Закрыть">&times;</span></div>');

  if( typeof(type) == 'undefined' ) type = 'notice';

  $('#global_notices').html(notice);

  notice.find('#notice_close').on('click', function(){
    notice.remove();
  })

  $(window).scrollTo(notice);

  if( typeof(duration) != 'undefined' ){
    setTimeout(function(){
      notice.remove();
    },duration);
  }
}
