/*
 * текстовые переменные
 */
var lang = {
  ru: {
    required_checkbox_not_checked : 'Необходимо принять пользовательское соглашение',
    required_field_empty : 'Необходимо заполнить поле',
    passwords_do_not_match : 'Введённые пароли не совпадают',
  },
  en: {
    required_checkbox_not_checked : 'You need to accept the User Agreement',
    required_field_empty : 'You need to fill in this field',
    passwords_do_not_match : 'Passwords you entered do not match',
  },
}

$(function() {
  /*
    Валидация полей формы
    - форма может быть валидируемой, а может и нет.
    за это отвечает класс .validateble;
    - на каждое поле в форме вешается обработчик который слушает события( onChange ).
    этот обработчик проверяет,
    -- onChange заполнено ли это поле, если нет, пишем ошибку ( только для полей с data-required="true" )
    -- onChange если есть удаленные валидации - запускаем их, если есть ошибки - выводим их
  */

  $(document).on('change', 'form.validateble input, form.validateble textarea, form.validateble select', function() {
    validateField($(this));
  })

  $(document).on('keyup', 'form.validateble input, form.validateble textarea', function(event){
    var $form = $(this).closest('form');
    var $field = $(this);

    if (event && event.keyCode !== 13){
      form_error_clear($form, $field);
    }

    toggleSubmitButton($form);
  })

  $(window).on('load', function(){
    var $form = $('form.validateble');
    toggleSubmitButton($form);
  })
})

/*
 * Валидация поля
 * проверяет сначала - обязательное ли это поле - если обязательное - проверяет заполнено ли оно. если не заполнено,
 * то выводит ошибку и дальше не проверяет. если все заполнено - идет проверять дальше. Дальше идет удаленная проверка
 * на сервере. Там делается запрос. Если все ок - то ничего не делаем. а если ошибка - то показывает ошибку.
 * Далее - если это поле пароль и оно имеет пару - проверяется верность введения паролей.
 * $field - object form field
 */

function validateField($field){
  var $form = $field.closest('form');
  var value = $field.val();
  var name = $field.attr('name');

  form_error_clear($form, $field);

  /* проверка на обязательность */
  if( typeof($field.data('required')) != 'undefined' ){
    if($field.data('required') == true){
      if( $field.attr('type') == 'checkbox'){
        if( !$field.prop('checked') ){
          form_error_show($form, $field, lang[window.g_hl].required_checkbox_not_checked );
          toggleSubmitButton($form);
          return false;
        }
      }else{
        if( $.trim(value.length) == 0 ){
          form_error_show($form, $field, lang[window.g_hl].required_field_empty );
          toggleSubmitButton($form);
          return false;
        }
      }
    }
  }

  /* проверка по удаленному урлу */
  if( typeof($field.data('validate_url')) != 'undefined' ){
    var url = $field.data('validate_url');
    if( $.trim(value) != '') {
      $.post(url, { value: value, name: name }, function(json) {
        if(json.messages !== 'ok'){
          form_errors_show($form, json.errors );
        }

        form_error_clear($form, $field);
        toggleSubmitButton($form);
      })
    }
  }
  toggleSubmitButton($form);
}

/**
 * Задизебливание кнопки сабмита
 * - кнопка сабмита может быть дизеблимой, а может и нет.
 * за это отвечает класс .disableble;
 * - кнопка сабмита должна быть задизебленной, если в форме есть хотя бы одно не заполненое обязательное поле
 * обязательные поля помечаются как data-required="true";
 * - кнопка сабмита должна быть задизебленной, если в форме есть хоть одна ошибка
 * - дизебливание кнопки проверяется после каждого onChange или onKeypress события в форме, после того, как отработают все валидации.
 */

function toggleSubmitButton($form){
  var disabled_submit = false;
  $form.find('[data-required="true"]').each(function(i, field){
    if( field.type == 'checkbox'){
      if( !$(field).prop('checked') ){
        disabled_submit = true;
      }
    }else{
      if( $.trim($(field).val()) == '' ){
        disabled_submit = true;
      }
    }
  })
  if( $form.find('.field-with-error, .s-with-error').length){
    disabled_submit = true;
  }
  $form.find('.disableble').prop('disabled', disabled_submit);
}

/*
 * показ ошибок в стандартных формах
 * @form - string - селектор формы
 * @errors - array/json - массив ошибок {'field_name':'text error', ... }
 */

function form_errors_show(form, errors) {
  var $form = $(form);
  $.each(errors, function(name, text){
    var $field = $form.find('[name="'+name+'"]');
    form_error_show($form, $field, text);
  })
}

/*
 * Показ ошибки в поле
 * $form - form object
 * $field - field object
 * text - string
 */

function form_error_show($form, $field, text) {
  var field;
  if($form.hasClass('s-form')){
    field = $field.closest('.s-field');
    field.addClass('s-with-error');
    field.append('<div class="s-error">' + text + '</div>');
  }else{
    field = $field.closest('.field');
    field.find('.field-content').addClass('field-with-error');
    field.append('<div class="field-error">' + text + '</div>');
  }
}

/*
 * очищение формы от ошибок в стандартных формах
 * @form - string - селектор формы
 */

function form_errors_clear(form){
  var $form = $(form);
  if($form.hasClass('s-form')){
    $form.find('.s-with-error').removeClass('s-with-error');
    $form.find('.s-error').remove();
  }else{
    $form.find('.field-with-error').removeClass('field-with-error');
    $form.find('.field-error').remove();
  }
}

/*
 * очищение поля
 * @form - object - селектор формы
 * @field - object -  поля
 */

function form_error_clear($form, $field){
  if($form.hasClass('s-form')){
    var $field = $field.closest('.s-field');
    $field.removeClass('s-with-error');
    $field.find('.s-error').remove();
  }else{
    var $field = $field.closest('.field');
    $field.find('.field-content').removeClass('field-with-error');
    $field.find('.field-error').remove();
  }
}
