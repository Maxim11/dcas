-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Мар 06 2022 г., 09:33
-- Версия сервера: 5.7.21-20-beget-5.7.21-20-1-log
-- Версия PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `l9522810_lumex`
--

-- --------------------------------------------------------

--
-- Структура таблицы `devices`
--
-- Создание: Сен 16 2021 г., 19:40
--

DROP TABLE IF EXISTS `devices`;
CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `device_type_id` int(11) NOT NULL,
  `serial_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `devices`
--

INSERT INTO `devices` (`id`, `device_type_id`, `serial_number`) VALUES
(1, 1, '1232020'),
(2, 1, '1252020'),
(3, 1, '2402020'),
(4, 3, '20320020'),
(5, 3, '2062020'),
(6, 3, '2032020'),
(7, 4, '2172020'),
(8, 4, '2342012'),
(9, 4, '12314442');

-- --------------------------------------------------------

--
-- Структура таблицы `device_type`
--
-- Создание: Сен 23 2021 г., 16:16
--

DROP TABLE IF EXISTS `device_type`;
CREATE TABLE `device_type` (
  `id` int(11) NOT NULL,
  `type_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `device_type`
--

INSERT INTO `device_type` (`id`, `type_name`) VALUES
(1, 'МГА-1000'),
(3, 'Капель-105'),
(4, 'Капель-205');

-- --------------------------------------------------------

--
-- Структура таблицы `history`
--
-- Создание: Янв 04 2022 г., 10:10
--

DROP TABLE IF EXISTS `history`;
CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `instrument_type` varchar(45) NOT NULL,
  `serial_number` varchar(45) NOT NULL,
  `user_uploaded_file` varchar(45) NOT NULL,
  `software_versions` varchar(45) NOT NULL,
  `upload_date` datetime NOT NULL,
  `found_string` varchar(255) NOT NULL,
  `critical_errors` varchar(45) NOT NULL,
  `error_description` varchar(255) DEFAULT NULL,
  `deviation_values` varchar(45) NOT NULL,
  `data_file` varchar(255) DEFAULT NULL,
  `user_action` varchar(255) DEFAULT NULL,
  `action_for_engineer` varchar(255) DEFAULT NULL,
  `engineer_file` varchar(255) DEFAULT NULL,
  `client_file` varchar(255) DEFAULT NULL,
  `alert` varchar(11) DEFAULT NULL,
  `commentary` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `history`
--

INSERT INTO `history` (`id`, `instrument_type`, `serial_number`, `user_uploaded_file`, `software_versions`, `upload_date`, `found_string`, `critical_errors`, `error_description`, `deviation_values`, `data_file`, `user_action`, `action_for_engineer`, `engineer_file`, `client_file`, `alert`, `commentary`) VALUES
(1, 'МГА-1000', '1232020', '', '', '0000-00-00 00:00:00', '[10/24/13 13:22:13] Error Atomizer: hardware error 9 in message 3', '', NULL, '', 'example1.log', 'Посмотри в зеркало', 'Почини прибор', 'doet.pdf', NULL, 'yes', NULL),
(2, 'МГА-1000', '23445454', '', '14', '0000-00-00 00:00:00', 'ааацуаcscsdc', 'not', 'нет', '', 'test.log', 'not', 'not', 'test.pdf', 'test.pdf', 'not', 'not'),
(3, 'МГА-1000', '1232020', '', '14', '0000-00-00 00:00:00', 'ааацуаcscsdc', 'not', 'нет', '', 'test.log', 'not', 'not', 'test.pdf', 'test.pdf', 'not', 'not');

-- --------------------------------------------------------

--
-- Структура таблицы `link_history_rules_type`
--
-- Создание: Сен 15 2021 г., 21:40
--

DROP TABLE IF EXISTS `link_history_rules_type`;
CREATE TABLE `link_history_rules_type` (
  `id` int(11) NOT NULL,
  `history_id` int(11) NOT NULL,
  `rules_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `link_history_rules_type`
--

INSERT INTO `link_history_rules_type` (`id`, `history_id`, `rules_type_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `link_history_user`
--
-- Создание: Сен 18 2021 г., 13:05
--

DROP TABLE IF EXISTS `link_history_user`;
CREATE TABLE `link_history_user` (
  `id` int(11) NOT NULL,
  `history_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `link_rules_device_type`
--
-- Создание: Сен 16 2021 г., 20:02
--

DROP TABLE IF EXISTS `link_rules_device_type`;
CREATE TABLE `link_rules_device_type` (
  `id` int(11) NOT NULL,
  `rules_id` int(11) NOT NULL,
  `devices_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `link_rules_device_type`
--

INSERT INTO `link_rules_device_type` (`id`, `rules_id`, `devices_type_id`) VALUES
(32, 60, 1),
(33, 61, 1),
(34, 62, 1),
(35, 63, 1),
(36, 64, 1),
(37, 65, 1),
(38, 66, 1),
(40, 68, 4),
(41, 69, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `link_rules_type_rules`
--
-- Создание: Сен 15 2021 г., 21:27
--

DROP TABLE IF EXISTS `link_rules_type_rules`;
CREATE TABLE `link_rules_type_rules` (
  `id` int(11) NOT NULL,
  `rules_type_id` int(11) NOT NULL,
  `rules_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `link_rules_type_rules`
--

INSERT INTO `link_rules_type_rules` (`id`, `rules_type_id`, `rules_id`) VALUES
(31, 1, 60),
(32, 1, 61),
(33, 2, 62),
(34, 1, 63),
(35, 1, 64),
(36, 1, 65),
(37, 1, 66),
(39, 1, 68),
(40, 1, 69);

-- --------------------------------------------------------

--
-- Структура таблицы `link_users_devices`
--
-- Создание: Сен 15 2021 г., 21:12
--

DROP TABLE IF EXISTS `link_users_devices`;
CREATE TABLE `link_users_devices` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `link_users_devices`
--

INSERT INTO `link_users_devices` (`id`, `user_id`, `device_id`) VALUES
(1, 1, 1),
(2, 3, 4),
(3, 4, 3),
(4, 5, 6),
(5, 1, 2),
(6, 1, 9),
(7, 4, 1),
(8, 4, 1),
(9, 4, 7),
(10, 4, 7);

-- --------------------------------------------------------

--
-- Структура таблицы `link_user_type_user`
--
-- Создание: Сен 14 2021 г., 18:08
--

DROP TABLE IF EXISTS `link_user_type_user`;
CREATE TABLE `link_user_type_user` (
  `id` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `link_user_type_user`
--

INSERT INTO `link_user_type_user` (`id`, `user_type_id`, `user_id`) VALUES
(1, 1, 1),
(2, 2, 3),
(5, 3, 4),
(6, 3, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `rules`
--
-- Создание: Ноя 23 2021 г., 10:03
--

DROP TABLE IF EXISTS `rules`;
CREATE TABLE `rules` (
  `id` int(11) NOT NULL,
  `serial_number_first` int(11) NOT NULL,
  `serial_number_last` varchar(45) NOT NULL,
  `file_type` varchar(45) DEFAULT NULL,
  `master_key` varchar(45) NOT NULL,
  `additional_key` varchar(45) DEFAULT NULL,
  `condition` varchar(255) NOT NULL,
  `conditionApp` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `user_action` varchar(255) DEFAULT NULL,
  `action_for_engineer` varchar(255) DEFAULT NULL,
  `engineer_file` varchar(255) DEFAULT NULL,
  `client_file` varchar(255) DEFAULT NULL,
  `alert` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `rules`
--

INSERT INTO `rules` (`id`, `serial_number_first`, `serial_number_last`, `file_type`, `master_key`, `additional_key`, `condition`, `conditionApp`, `description`, `user_action`, `action_for_engineer`, `engineer_file`, `client_file`, `alert`) VALUES
(60, 806, '', 'log', 'Error   Atomizer', 'idle', '1', '', 'Перегрев выпрямителя', 'Убедиться, что работает вентиллятор выпрямителя', 'Выполнить ремонт по инструкции', 'readmy.txt', 'Инстр.pdf', 'да'),
(61, 806, '', 'log', 'Error   Atomizer', 'sensor', '2', '', 'Отсутствие датчиков температуры инвертора', 'Проверить подключение и целостность кабелей 25 и 26', '', '', '', 'да'),
(62, 806, '1233434343', 'log', 'Info	Atomizer', 'v.', '1.3', '', 'Версия атомизатора устарела', 'Обновить текущую версию', 'Обновить текущую версию', '', '', 'да'),
(63, 800, '', 'log', 'DeviceConnect', '', 'Failed to open port', '', 'Обнаружен не открытый порт (DeviceConnect: Failed to open port)', 'Если не удаётся организовать связь прибор-компьютер', '', 'фирмы.txt', 'DeviceConnect Failed_to open port.txt', 'да'),
(64, 800, '', 'log', 'Temperature is out of calibration range', '', '', '', 'Один из этапов температурной программы требует задание мощности, вне допустимого диапазона (меньше Мин или больше Макс).', 'Проверьте для каждого этапа температурной программы, соответствует ли температура этапа температурной программы допустимому диапазону мощностей (больше Мин и меньше Макс). Мин и Макс мощности указаны во вкладке Аппаратные градуировки для 1-го и 2го типов ', '', '', '', 'да'),
(65, 800, '', 'log', 'Error	ADC:', '', 'hardware error 1 in message 11', '', 'Неверное количество шагов в круге.', 'Проверить аппаратную градуировку. Корректное значение 12800 шагов в круге.', '', '', '', 'да'),
(66, 800, '', 'log', 'Lamps turret:', '', 'hardware error 1 in message 6', '', 'Неверное количество шагов в круге.', 'Проверить аппаратную градуировку. Корректное значение 12800 шагов в круге.', '', '', '', 'нет'),
(68, 0, '', '', '', '', '', '6.1', '', 'описание', '', '', '', 'да'),
(69, 800, '122222222222222222222222222', 'log', 'Atomizer', 'hardware error', '3', '', 'Oops', 'Better call Saul', '', '', '', 'да');

-- --------------------------------------------------------

--
-- Структура таблицы `rules_type`
--
-- Создание: Сен 15 2021 г., 21:23
--

DROP TABLE IF EXISTS `rules_type`;
CREATE TABLE `rules_type` (
  `id` int(11) NOT NULL,
  `rules_type` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `rules_type`
--

INSERT INTO `rules_type` (`id`, `rules_type`) VALUES
(1, 'error'),
(2, 'version'),
(3, 'value');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--
-- Создание: Сен 14 2021 г., 18:05
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `e_mail` varchar(45) DEFAULT NULL,
  `region` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `pass`, `user_name`, `phone`, `e_mail`, `region`) VALUES
(1, 'Pavel', '213fasdf', 'Pavel Mikhnovets', NULL, NULL, NULL),
(3, 'User', 'ingener', 'User Ingener', '9544564556', 'ingener@yandex.ru', 'St. Petersburg'),
(4, 'Ivan', '1234', 'Ivan Client', '9532342345', 'client2@yandex.ru', 'St. Petersburg'),
(5, 'Stepan', '12345', 'Stepan Client', '9532342333', 'client1@yandex.ru', 'St. Petersburg');

-- --------------------------------------------------------

--
-- Структура таблицы `user_type`
--
-- Создание: Сен 12 2021 г., 15:19
--

DROP TABLE IF EXISTS `user_type`;
CREATE TABLE `user_type` (
  `id` int(11) NOT NULL,
  `user_type` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_type`
--

INSERT INTO `user_type` (`id`, `user_type`) VALUES
(1, 'administrator'),
(2, 'service_engineer'),
(3, 'client');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `device_type`
--
ALTER TABLE `device_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `link_history_rules_type`
--
ALTER TABLE `link_history_rules_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `link_history_user`
--
ALTER TABLE `link_history_user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `link_rules_device_type`
--
ALTER TABLE `link_rules_device_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `link_rules_type_rules`
--
ALTER TABLE `link_rules_type_rules`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `link_users_devices`
--
ALTER TABLE `link_users_devices`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `link_user_type_user`
--
ALTER TABLE `link_user_type_user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rules`
--
ALTER TABLE `rules`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rules_type`
--
ALTER TABLE `rules_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `device_type`
--
ALTER TABLE `device_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `link_history_rules_type`
--
ALTER TABLE `link_history_rules_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `link_history_user`
--
ALTER TABLE `link_history_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `link_rules_device_type`
--
ALTER TABLE `link_rules_device_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT для таблицы `link_rules_type_rules`
--
ALTER TABLE `link_rules_type_rules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT для таблицы `link_users_devices`
--
ALTER TABLE `link_users_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `link_user_type_user`
--
ALTER TABLE `link_user_type_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `rules`
--
ALTER TABLE `rules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT для таблицы `rules_type`
--
ALTER TABLE `rules_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
