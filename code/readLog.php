<?php
require_once "IReading.php";

class ReadingLog implements IReading
{
    public function __construct()
    {
        $this->ext = 'log';
    }

    public function read($filePath, $key, $condition)
    {
        $handle = fopen($filePath, "r+");
        $readResult = NULL;
        while (($parsedString = fgets($handle, 4096)) !== false)
        {
            if (preg_match('/^(\\d)/ui', $condition)) {
                if (preg_match('/\b' . $key . '/ui', $parsedString)) {
                    $strKeys = preg_split("/[\t,:\s]+/", $key, -1, PREG_SPLIT_NO_EMPTY);
                    $parsedArray = preg_split("/[\\[\\]\t,:\s]+/", $parsedString, -1, PREG_SPLIT_NO_EMPTY);
                    $result = array_intersect($parsedArray, $strKeys);

                    $readResult = $parsedArray[array_key_last($result) + 1];
                    break;
                }
            }

            if (preg_match('/\\</ui', $condition)) {
                echo 'Ok';
                break;
            }
            if (preg_match('/\\>/ui', $condition)) {
                echo 'Ok';
                break;
            }
            if (preg_match('/(!=)/ui', $condition)) {
                echo 'Ok';
                break;
            }
            /*foreach ($strKeys as $key => $strKey) {
                if (preg_match('/\b' . $strKey . '\b/ui', $parsedString))
                {
                    if ($key == array_key_last($strKeys))
                    {
                        $parsedArray = preg_split("/[\\[\\]\t,\s]+/", $parsedString, -1, PREG_SPLIT_NO_EMPTY);

                        foreach ($parsedArray as $key => $strCondition) {

                            if (preg_match('/\b' . $strKey . '\b/ui', $strCondition))
                            {
                                if ($condition == $parsedArray[$key + 1])
                                {
                                    $readResult = "Ошибка найдена";
                                }
                            }
                        }
                    }
                }
                else break;
            }*/
        }

        fclose($handle);

        return $readResult ?? 'numErrorNotFound';
    }
}

$read = new ReadingLog();
//$keywords = preg_split("/[\\[\\]\t,\s]+/", "[01/22/19 08:23:43] Debug	ED-lamp: SyncTask complete", -1, PREG_SPLIT_NO_EMPTY);
//print_r($keywords);
//$key = 'Error   Atomizer idle:';
//$key = 'Info	Atomizer v.';
$key = 'Info	Atomizer v.';
$condition = '2.2';
echo gettype($condition), "\n";
echo $read->read('./example.log', $key, $condition);