<?php

include "controller/ProcessingRulesController.php";

//<li><a href="https://www.lumex.ru/help.php">Обратная связь</a></li>	
$giveRules = new ProcessingRulesController();

if($_POST['send'] === 'save')
{
    //echo $_FILES['fileClient']['name'];
   $giveRules->createRule();
}

if($_POST['send'] === 'update')
{
   $giveRules->updateForm($_POST['ruleId']);
}

if($_POST['send'] === 'delete')
{
   $giveRules->deleteRule($_POST['ruleId'], [$_POST['clientFile'], $_POST['engineerFile']]);
}

if($_POST['send'] === 'updateRule')
{
   $giveRules->updateRule($_POST['ruleId']);
}

if($_POST['read'])
{
   $giveRules->readFile($_POST['read']);
}

$giveRules->run($id, $table_name);    