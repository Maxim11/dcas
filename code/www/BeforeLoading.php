<?php
include "controller/BeforeLoadingController.php";

$received_data = json_decode(file_get_contents("php://input"), true) ?: 0;
    
$id = $received_data[id];
$table_name = $received_data[request_for];
$giveLoading = new BeforeLoadingController();	
    
$giveLoading->run($id, $table_name);