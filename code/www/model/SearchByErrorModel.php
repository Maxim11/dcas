<?php
                                
include "BaseModel.php";

class SearchByErrorModel extends BaseModel
{	
	public function __construct()
	{
        parent::__construct();		
	}

	public function getDevicesData($id, $table_name)
	{
	   /* echo $id;
	    echo "/n";
	    echo $table_name;*/
	    $userId = 1;//
	    
	    if($table_name == 'device_type')
        {
            $sqln = $this->connection->query('
            SELECT DISTINCT dt.id, dt.type_name
            FROM `rules` r, `link_rules_device_type`lrd, `rules_type` rt, `link_rules_type_rules` lrtr, `device_type` dt
            LEFT JOIN `devices` AS d ON dt.id=d.device_type_id
            LEFT JOIN `link_users_devices` lud ON lud.device_id=d.id
            LEFT JOIN `users` ON users.id=lud.user_id
            WHERE '.$userId.'
            AND r.serial_number_first<=d.serial_number
            AND r.serial_number_last>=d.serial_number
            AND lrd.devices_type_id=dt.id
            AND r.id=lrd.rules_id
            AND lrtr.rules_id=r.id
            AND rt.id=lrtr.rules_type_id
            AND r.serial_number_first<=d.serial_number
            AND r.serial_number_last>=d.serial_number
            AND lrd.devices_type_id=dt.id
            AND r.id=lrd.rules_id
            AND lrtr.rules_id=r.id
            AND rt.id=lrtr.rules_type_id
            AND rt.id=1
            ');
        }    
    
        if($table_name == 'devices')
        {
            $sqln = $this->connection->query('
            SELECT DISTINCT d.id, d.serial_number
            FROM `rules` r, `link_rules_device_type`lrd, `rules_type` rt, `link_rules_type_rules` lrtr, `device_type` dt
            LEFT JOIN `devices` AS d ON dt.id=d.device_type_id
            LEFT JOIN `link_users_devices` lud ON lud.device_id=d.id
            LEFT JOIN `users` ON users.id=lud.user_id
            WHERE '.$userId.'
            AND r.serial_number_first<=d.serial_number
            AND r.serial_number_last>=d.serial_number
            AND lrd.devices_type_id=dt.id
            AND r.id=lrd.rules_id
            AND lrtr.rules_id=r.id
            AND rt.id=lrtr.rules_type_id
            AND rt.id=1
            AND dt.id = '.$id.'            
            ');
        }    
        
        if($table_name == 'rules')
        {
            $sqln = $this->connection->query('
            SELECT DISTINCT r.id, r.conditionApp
            FROM `rules` r, `link_rules_device_type`lrd, `rules_type` rt, `link_rules_type_rules` lrtr, `device_type` dt
            LEFT JOIN `devices` AS d ON dt.id=d.device_type_id
            LEFT JOIN `link_users_devices` lud ON lud.device_id=d.id
            LEFT JOIN `users` ON users.id=lud.user_id
            WHERE '.$userId.'
            AND r.serial_number_first<=d.serial_number
            AND r.serial_number_last>=d.serial_number
            AND lrd.devices_type_id=dt.id
            AND r.id=lrd.rules_id
            AND lrtr.rules_id=r.id
            AND rt.id=lrtr.rules_type_id
            AND rt.id=1
            AND d.id = '.$id.'           
            ');
        }    
        /*$sqln = $this->connection->query('SELECT DISTINCT dt.type_name, d.serial_number, r.condition
            FROM `rules` r, `link_rules_device_type`lrd, `rules_type` rt, `link_rules_type_rules` lrtr, `device_type` dt
            LEFT JOIN `devices` AS d ON dt.id=d.device_type_id
            LEFT JOIN `link_users_devices` ON link_users_devices.device_id=d.id
            LEFT JOIN `users` ON users.id=link_users_devices.user_id
            WHERE users.id=1
            AND dt.id=d.device_type_id
            AND lrd.devices_type_id=dt.id
            AND r.id=lrd.rules_id
            AND lrtr.rules_id=r.id
            AND rt.id=lrtr.rules_type_id
            AND rt.id=1
            AND r.serial_number_first<=d.serial_number
            AND r.serial_number_last>=d.serial_number
        ');
        */
        //var_dump  ($sqln ? $sqln->fetchAll(PDO::FETCH_OBJ) : 0);
		 return  $sqln ? $sqln->fetchAll(PDO::FETCH_OBJ) : 0;
	}
	
}