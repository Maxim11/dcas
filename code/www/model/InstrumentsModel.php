<?php
                                
include "BaseModel.php";

class InstrumentsModel extends BaseModel
{	
	public function __construct()
	{
        parent::__construct();		
	}

	public function getInstruments($id, $table_name)
	{
	    $userId = 1;//
	    
	    if($table_name == 'device_type')
        {
            $sqln = $this->connection->query('
            SELECT * FROM `device_type` WHERE 1
            ');
        }    
    
        if($table_name == 'devices')
        {
            $sqln = $this->connection->query('
            SELECT DISTINCT d.id, d.serial_number
            FROM `devices` d
            LEFT JOIN `device_type` AS dt ON dt.id=d.device_type_id
            WHERE dt.id='.$id.'
            ');
        }    
        
        if($table_name == 'users')
        {
            $sqln = $this->connection->query('
            SELECT DISTINCT u.id, u.user_name
            FROM `users` u
            LEFT JOIN link_users_devices lud ON u.id=lud.user_id
            LEFT JOIN `devices` AS d ON lud.device_id=d.id
            WHERE d.id='.$id.'
            ');
        }    
        
		 return  $sqln ? $sqln->fetchAll(PDO::FETCH_OBJ) : 0;
	}
	
}