<?php
                                
include "BaseModel.php";

class BeforeLoadingModel extends BaseModel
{	
	public function __construct()
	{
        parent::__construct();		
	}

	public function getDevicesData($id, $table_name)
	{
	    if($table_name == 'device_type')
        {
            $sqln = $this->connection->query('
            SELECT DISTINCT dt.id, dt.type_name
            FROM `device_type` dt
            LEFT JOIN `devices` AS d ON dt.id=d.device_type_id
            LEFT JOIN `link_users_devices` lud ON lud.device_id=d.id
            LEFT JOIN `users` ON users.id=lud.user_id
            WHERE users.id=1
            ');
        }    
    
        if($table_name == 'devices')
        {
            $sqln = $this->connection->query('
            SELECT DISTINCT d.id, d.serial_number
            FROM `device_type` dt
            LEFT JOIN `devices` AS d ON dt.id=d.device_type_id
            LEFT JOIN `link_users_devices` lud ON lud.device_id=d.id
            LEFT JOIN `users` ON users.id=lud.user_id
            WHERE users.id=1
            AND dt.id = '.$id.'            
            ');
        }    
        
		return  $sqln ? $sqln->fetchAll(PDO::FETCH_OBJ) : 0;
	}
	
}