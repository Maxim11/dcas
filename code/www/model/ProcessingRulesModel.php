<?php

require_once('BaseModel.php');                                
//include "BaseModel.php";

class ProcessingRulesModel extends BaseModel
{	
	public function __construct()
	{
        parent::__construct();		
	}

	public function getRules($ruleId, $table_name)
	{
	    $currentRuleId = (empty($ruleId)) ? 1 : 'r.id = '.$ruleId.'';
	    //echo $condition;
        $sqln = $this->connection->query('
            SELECT DISTINCT r.*, rt.rules_type, dt.type_name
            FROM  link_rules_device_type lrdt, device_type dt, rules r
            LEFT JOIN link_rules_type_rules lrtr ON lrtr.rules_id = r.id
            LEFT JOIN rules_type rt ON rt.id = lrtr.rules_type_id
            WHERE '.$currentRuleId.'
            AND r.id = lrdt.rules_id
            AND dt.id = lrdt.devices_type_id

        ');
           
		return  $sqln ? $sqln->fetchAll(PDO::FETCH_OBJ) : 0;
	}
	
	public function createRule()
	{
	    if(isset($_POST['ruleTypeId'], $_POST['instrumentTypesId']))
	    {
	        $initialSerialNumber = $_POST['initialSerialNumber'];
            $endSerialNumber = $_POST['endSerialNumber'];
            $fileType = $_POST['fileType'];
            $masterKey = $_POST['masterKey'];
            $additionalKey = $_POST['additionalKey'];
            $condition = $_POST['condition'];
            $conditionApp = $_POST['conditionApp'];
            $description = $_POST['description'];
            $userAction = $_POST['userAction'];
            $engineerAction = $_POST['engineerAction'];
    	    $fileClient = $_FILES['fileClient']['name'];
    	    $fileEngineer = $_FILES['fileEngineer']['name'];
    	    $alert = $_POST['alert'];
    	
        	$this->connection->exec('SET NAMES utf8 COLLATE utf8_unicode_ci');
    	
    	/*$sql="INSERT INTO user_type (user_type) 
            values (:user_type)";*/
    
            $sql="INSERT INTO rules (serial_number_first, serial_number_last, file_type, master_key, additional_key,`condition`, conditionApp,
                    `description`, user_action, action_for_engineer, engineer_file, client_file, alert) 
                
                    values (:initialSerialNumber,:endSerialNumber, :fileType, :masterKey, :additionalKey, :condition, :conditionApp,
                    :description, :userAction, :engineerAction, :fileEngineer, :fileClient, :alert)";
                
            $sth = $this->connection->prepare($sql);
        //$sth->bindValue(':user_type', $deviceType);
            $sth->bindValue(':initialSerialNumber', $initialSerialNumber);
            $sth->bindValue(':endSerialNumber', $endSerialNumber);
            $sth->bindValue(':fileType', $fileType);
            $sth->bindValue(':masterKey', $masterKey);
            $sth->bindValue(':additionalKey', $additionalKey);
            $sth->bindValue(':condition', $condition);
            $sth->bindValue(':conditionApp', $conditionApp);
            $sth->bindValue(':description', $description);
            $sth->bindValue(':userAction', $userAction);
            $sth->bindValue(':engineerAction', $engineerAction);
            $sth->bindValue(':fileEngineer', $fileEngineer);
            $sth->bindValue(':fileClient', $fileClient);
            $sth->bindValue(':alert', $alert);
        
        //$sth->execute();	
        
            if ($sth->execute()) 
            {
                $lastId = $this->connection->lastInsertId();
                $this->createLinkRuleDeviceType ($lastId, $_POST['instrumentTypesId']);
                $this->createLinkRuleTypeRule ($lastId, $_POST['ruleTypeId']);
                //header("Location: http://l9522810.beget.tech/processingRules.php");
                  //echo "New record created successfully";
            }
        
        /*else 
        {
            $arr = $sth->errorInfo();
            print_r($arr);
            
            echo "Unable to create record";
        }*/
	    }
	}
	
	public function createLinkRuleDeviceType($ruleId, $deviceType)
	{
	    $sql="INSERT INTO link_rules_device_type (rules_id, devices_type_id) values (:ruleId,:deviceType)";
                
        $sth = $this->connection->prepare($sql);
        //$sth->bindValue(':user_type', $deviceType);
        $sth->bindValue(':ruleId', $ruleId);
        $sth->bindValue(':deviceType', $deviceType);
        
        $sth->execute();
	}
	
	public function createLinkRuleTypeRule($ruleId, $ruleType)
	{
        $sql="INSERT INTO link_rules_type_rules (rules_type_id, rules_id) values (:ruleType,:ruleId)";
	   
        $sth = $this->connection->prepare($sql);
        //$sth->bindValue(':user_type', $deviceType);
        $sth->bindValue(':ruleId', $ruleId);
        $sth->bindValue(':ruleType', $ruleType);
	
	    $sth->execute();
        
	}
	
	public function updateRule($ruleId)
	{
	        $initialSerialNumber = $_POST['initialSerialNumber'];
            $endSerialNumber = $_POST['endSerialNumber'];
            $fileType = $_POST['fileType'];
            $masterKey = $_POST['masterKey'];
            $additionalKey = $_POST['additionalKey'];
            $condition = $_POST['condition'];
            $conditionApp = $_POST['conditionApp'];
            $description = $_POST['description'];
            $userAction = $_POST['userAction'];
            $engineerAction = $_POST['engineerAction'];
    	    $fileClient = $_FILES['fileClient']['name'] ?: $_POST['fileClientBase'];
    	    $fileEngineer = $_FILES['fileEngineer']['name'] ?: $_POST['fileEngineerBase'];
    	    $alert = $_POST['alert'];
    	    
        	$this->connection->exec('SET NAMES utf8 COLLATE utf8_unicode_ci');
    	
            $sql='UPDATE rules SET serial_number_first=:initialSerialNumber, serial_number_last=:endSerialNumber, file_type=:fileType, master_key=:masterKey,
            additional_key=:additionalKey,`condition`=:condition, `conditionApp`=:conditionApp,
                    `description`=:description, user_action=:userAction, action_for_engineer=:engineerAction,
                    engineer_file=:fileEngineer, client_file=:fileClient, alert=:alert WHERE rules.id =  '.$ruleId.'';
            
            $sth = $this->connection->prepare($sql);
            //print_r ($sth);
        //$sth->bindValue(':user_type', $deviceType);
            /*$sth->bindValue(':initialSerialNumber', $initialSerialNumber);
            $sth->bindValue(':endSerialNumber', $endSerialNumber);
            $sth->bindValue(':fileType', $fileType);
            $sth->bindValue(':masterKey', $masterKey);
            $sth->bindValue(':additionalKey', $additionalKey);
            $sth->bindValue(':condition', $condition);
            $sth->bindValue(':description', $description);
            $sth->bindValue(':userAction', $userAction);
            $sth->bindValue(':engineerAction', $engineerAction);
            $sth->bindValue(':fileEngineer', $fileEngineer);
            $sth->bindValue(':fileClient', $fileClient);
            $sth->bindValue(':alert', $alert);*/
            
            $sth->execute(array(
                ':initialSerialNumber' => $initialSerialNumber,
                ':endSerialNumber' => $endSerialNumber,
                ':fileType' => $fileType,
                ':masterKey' => $masterKey,
                ':additionalKey' => $additionalKey,
                ':condition' => $condition,
                ':conditionApp' => $conditionApp,
                ':description' => $description,
                ':userAction' => $userAction,
                ':engineerAction' => $engineerAction,
                ':fileEngineer' => $fileEngineer,
                ':fileClient' => $fileClient,
                ':alert' => $alert
			));
            /*$arr = $sth->errorInfo();
            print_r($arr);*/
            
                $this->updateLinkRuleDeviceType ($ruleId, $_POST['instrumentTypesId']);
                $this->updateLinkRuleTypeRule ($ruleId, $_POST['ruleTypeId']);
                
                return 0;
            
	}
	
	public function updateLinkRuleDeviceType($ruleId, $deviceType)
	{
	    if(is_numeric($deviceType))
	    {
	        $sql='UPDATE link_rules_device_type SET devices_type_id=:deviceType WHERE link_rules_device_type.rules_id = '.$ruleId.'';
                
            $sth = $this->connection->prepare($sql);
        //$sth->bindValue(':user_type', $deviceType);
            $sth->execute(array(
            ':deviceType'=> $deviceType
            ));
        
            $sth->execute();
	    }
        return 0;
	}
	
	public function updateLinkRuleTypeRule($ruleId, $ruleType)
	{
	    if(is_numeric($ruleType))
	    {
	        $sql='UPDATE link_rules_type_rules SET rules_type_id=:ruleType WHERE link_rules_type_rules.rules_id = '.$ruleId.'';
	   
            $sth = $this->connection->prepare($sql);
            $sth->execute(array(
        
            ':ruleType'=> $ruleType
            ));
	
	        $sth->execute();
	    }
	    
	    return 0;
	}
	
	public function deleteRule($ruleId)
	{
	    $sqln = $this->connection->query('
            DELETE FROM `rules` WHERE `rules`.`id` = '.$ruleId.'

        ');
        
        $this->deleteLinkRuleDeviceType ($ruleId);
        $this->deleteLinkRuleTypeRule ($ruleId);
		return  0;
	}
	
	public function deleteLinkRuleDeviceType($ruleId)
	{
	    $sqln = $this->connection->query('
            DELETE FROM `link_rules_device_type` WHERE `link_rules_device_type`.`rules_id` = '.$ruleId.'
        ');
        
        return 'deleteLinkRuleDeviceType';
	}
	
	public function deleteLinkRuleTypeRule($ruleId)
	{
	    $sqln = $this->connection->query('
            DELETE FROM `link_rules_type_rules` WHERE `link_rules_type_rules`.`rules_id` = '.$ruleId.'
        ');
        
        return 'deleteLinkRuleTypeRule';
	}
}