<?php

include "Config.php";

class BaseModel
{
    public $connection;
    public $baseDir = './uploads';

    function __construct()
    {
        $this->connection = new PDO('mysql:host=' . Config::$host . ';dbname=' . Config::$dbname . ";charset=utf8", Config::$login, Config::$pass);
        $this->connection->exec('SET NAMES utf8 COLLATE utf8_unicode_ci');
    }
    
    public function getData($filePath)
    {
        if (file_exists($filePath)) {

            return simplexml_load_file($filePath);;

        }
        else {
            echo "Файл: " . $filePath . " не существует";
            exit;
        }
    }
    
    public function upload()
	{
	    $uploaddir = $this->baseDir; // . - текущая папка где находится submit.php
    
    // cоздадим папку если её нет
    if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );

    $files = $_FILES; // полученные файлы
    $done_files = array();

    // переместим файлы из временной директории в указанную
    foreach( $files as $file ){
        
        $file_name = $file['name'];

        if( move_uploaded_file( $file['tmp_name'], "$uploaddir/$file_name" ) ){
            //$done_files[] = realpath( "$uploaddir/$file_name" );
            $done_files[] = $file_name;
        }
    }
    
    $data = $done_files ? array('files' => $done_files ) : array('error' => 'Ошибка загрузки файлов.');
    
    return $data;
	}
	
	public function deleteFiles(array $files)
	{
	    foreach($files as $file)
	    {
	        unlink($this->baseDir.'/'.$file);
	    }
	    
	    return 0;
	}

    /**
     * @return string
     */
    public function getDevice($deviceId)
    {
        $sql = $this->connection->query('
            SELECT * FROM `devices` WHERE devices.id = '.$deviceId.'
        ');

        return $sql ? $sql->fetchAll(PDO::FETCH_OBJ) : 0;
    }
}