<?php
                                
require_once('BaseModel.php');

class AnalysisModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getRules($device_typeId, $devicesId)
    {
        //echo 1; exit;
        $sqln = $this->connection->query('
            SELECT DISTINCT rt.rules_type, d.serial_number,  r.*
            FROM   devices d, device_type dt 
            LEFT JOIN link_rules_device_type lrdt ON lrdt.devices_type_id = dt.id
            LEFT JOIN rules r ON lrdt.rules_id = r.id
            LEFT JOIN link_rules_type_rules lrtr ON lrtr.rules_id = r.id
            LEFT JOIN rules_type rt ON rt.id = lrtr.rules_type_id
            WHERE dt.id = '.$device_typeId.'
            
            AND d.device_type_id = dt.id
            AND d.id = '.$devicesId.'
            AND r.serial_number_first<=d.serial_number
            
        ');
//AND r.serial_number_last>=d.serial_number
        return $sqln ? $sqln->fetchAll(PDO::FETCH_OBJ) : 0;
    }

    public function analysisRun($fileName, $rulesData)
    {
        //$ext = 'log';
        $analysisResult = [];

//var_dump($rulesData);
        $filePath = "$this->baseDir" . '/' . "$fileName";
        foreach($rulesData as $ruleData)
        {
            $currentVersion = '';
            $currentError = '';
            $masterKey = $ruleData->master_key;//'Error Atomizer';
            $additionalKey = $ruleData->additional_key;//'runtime';
            $condition = $ruleData->condition;//'1';
            $fullKey = $additionalKey ? "$masterKey" ." ". "$additionalKey" : $masterKey;
            $handle = fopen($filePath, "r+");
            $strKeys = preg_split("/[\t,:\s]+/", $fullKey, -1, PREG_SPLIT_NO_EMPTY);


            while (($parsedString = fgets($handle, 4096)) !== false)
            {
                $parsedArray = preg_split("/[\\[\\]\t,:\s]+/", $parsedString, -1, PREG_SPLIT_NO_EMPTY);
                //if (preg_match('/^(\\d)/ui', $condition))
                $coincidenceArray =  array_intersect($strKeys, $parsedArray);
                //if(preg_match('/\b' . $masterKey . '/ui', $parsedString) && preg_match('/\b' . $additionalKey . '/ui', $parsedString))
                if(count($coincidenceArray) == count($strKeys))
                {
                    $convergence = array_intersect($parsedArray, $strKeys);

                    if($ruleData->rules_type === "error")
                    {
                        if(!(preg_match('/^[0-9.,]+$/ui', $condition)))
                        {
                            $arrayCodition = preg_split("/[\t,:\s]+/", $condition, -1, PREG_SPLIT_NO_EMPTY);
                            $coincidenceCondition = array_intersect($arrayCodition, $parsedArray);

                            if(count($arrayCodition) == count($coincidenceCondition))
                            {
                                $currentError = $ruleData;
                            }
                        }

                        else
                        {
                            $coincidence = $parsedArray[array_keys($convergence)[count($convergence)-1] + 1] == $condition ? 1 : 0;

                            if($coincidence)
                            {
                                $currentError = $ruleData;
                                //$analysisResult[]
                            }
                        }

                    }

                    if($ruleData->rules_type === "version")
                    {

                        $coincidence = $parsedArray[array_keys($convergence)[count($convergence)-1] + 1] < $condition ? 1 : 0;

                        if($coincidence)
                        {
                            //echo $parsedArray[array_keys($convergence)[count($convergence)-1] + 1];
                            $currentVersion = $ruleData;
                        }

                        else
                        {
                            //echo $parsedString;
                            $currentVersion = '';
                        }

                    }

                    if ($ruleData->rules_type === "value")
                    {
                        if (preg_match('/\\</ui', $condition))
                        {
                            $coincidence = $parsedArray[array_keys($convergence)[count($convergence)-1] + 1] < $condition ? 1 : 0;

                            if ($coincidence)
                            {
                                $analysisResult[] = $ruleData;
                            }
                            //echo 'Ok';
                            //break;
                        }

                        if (preg_match('/\\>/ui', $condition))
                        {
                            $coincidence = $parsedArray[array_keys($convergence)[count($convergence)-1] + 1] > $condition ? 1 : 0;

                            if ($coincidence)
                            {
                                $analysisResult[] = $ruleData;
                            }
                            //echo 'Ok';
                            //break;
                        }

                        if (preg_match('/(!=)/ui', $condition))
                        {
                            $coincidence = $parsedArray[array_keys($convergence)[count($convergence)-1] + 1] != $condition ? 1 : 0;

                            if ($coincidence)
                            {
                                $analysisResult[] = $ruleData;
                            }
                            //echo 'Ok';
                            //break;
                        }

                        $coincidence = $parsedArray[array_keys($convergence)[count($convergence)-1] + 1] == $condition ? 1 : 0;

                            if ($coincidence)
                            {
                                $analysisResult[] = $ruleData;
                            }

                        //preg_replace('/[^0-9]/i', '', $condition);

                    }
                    /*$strKeys = preg_split("/[\t,:\s]+/", $key, -1, PREG_SPLIT_NO_EMPTY);
                    $parsedArray = preg_split("/[\\[\\]\t,:\s]+/", $parsedString, -1, PREG_SPLIT_NO_EMPTY);
                    $result = array_intersect($parsedArray, $strKeys);

                    break;*/
                }
                //}

            }

            $analysisResult[] = $currentVersion ?: 0;
            $analysisResult[] = $currentError ?: 0;
            
            fclose($handle);
        }

        return $analysisResult;
    }
}
/*public function getDataAnalysis($rules)
{
    $key = 'Error Atomizer idle:';

    $condition = '1';

    $parsedString = "[10/20/15 09:56:15] Error Atomizer idle: 1 sensor: 0 runtime: 0";

    $strKeys = preg_split("/[\t,:\s]+/", $key, -1, PREG_SPLIT_NO_EMPTY);

    foreach ($strKeys as $key => $strKey)
    {
        if (preg_match('/\b' . $strKey . '\b/ui', $parsedString))
        {
            if ($key == array_key_last($strKeys))
            {
                $parsedArray = preg_split("/[\\[\\]\t,\s]+/", $parsedString, -1, PREG_SPLIT_NO_EMPTY);

                foreach ($parsedArray as $key => $strCondition)
                {

                    if (preg_match('/\b' . $strKey . '\b/ui', $strCondition))
                    {
                        if ($condition == $parsedArray[$key + 1]) echo "Ошибка найдена";
                    }
                }
            }
        } else break;
    }
}*/