<?php
require_once('BaseModel.php');                                
//include "BaseModel.php";

class InstrumentTypeModel extends BaseModel
{	
	public function __construct()
	{
        parent::__construct();		
	}

	public function getInstrumentType($id)
	{
        $sqln = $this->connection->query('
            SELECT * FROM `device_type`
        ');
           
		return  $sqln ? $sqln->fetchAll(PDO::FETCH_OBJ) : 0;
	}
}