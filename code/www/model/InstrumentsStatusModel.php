<?php

require_once('BaseModel.php');
//include "BaseModel.php";

class InstrumentsStatusModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getInstrumentsStatus($id)
    {
        $sqln = $this->connection->query('
            SELECT * FROM `history`
        ');
//var_dump($sqln->fetchAll(PDO::FETCH_OBJ)); exit;
        return  $sqln ? $sqln->fetchAll(PDO::FETCH_OBJ) : 0;
    }

    public function getInstrumentHistory($serialNumber)
    {
        $sqln = $this->connection->query('
            SELECT * FROM `history` 
            WHERE `history`.`serial_number`= '.$serialNumber.'
        ');
//var_dump($sqln->fetchAll(PDO::FETCH_OBJ)); exit;
        return  $sqln ? $sqln->fetchAll(PDO::FETCH_OBJ) : 0;
    
    }
}