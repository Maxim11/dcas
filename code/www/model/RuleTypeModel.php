<?php

require_once('BaseModel.php');                                
//include "BaseModel.php";

class RuleTypeModel extends BaseModel
{	
	public function __construct()
	{
        parent::__construct();		
	}

	public function getRuleType($id)
	{
        $sqln = $this->connection->query('
            SELECT * FROM `rules_type`
        ');
           
		return  $sqln ? $sqln->fetchAll(PDO::FETCH_OBJ) : 0;
	}
}	