<?php
                                
include "BaseModel.php";

class ErrorDescriptionModel extends BaseModel
{	
	public function __construct()
	{
        parent::__construct();		
	}

	public function getRuleData($rulesId, $devicesId)
	{
        $sqln = $this->connection->query('
            SELECT DISTINCT r.description, r.action_for_engineer, r.engineer_file, d.serial_number
            FROM `rules` r, `devices` d            
            WHERE r.id = '.$rulesId.' 
            AND d.id = '.$devicesId.'
        ');
    
         return $sqln ? $sqln->fetchAll(PDO::FETCH_OBJ) : 0;
	}
	
}