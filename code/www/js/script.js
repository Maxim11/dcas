$(document).ready(function(){
	var dropZone = $('#upload-container');
	var serializeFormData = $('#upload-container').serialize();

	$('#file-input').focus(function() {
		$('label').addClass('focus');
	})
	.focusout(function() {
		$('label').removeClass('focus');
	});


	dropZone.on('drag dragstart dragend dragover dragenter dragleave drop', function(){
		return false;
	});

	dropZone.on('dragover dragenter', function() {
		dropZone.addClass('dragover');
	});

	dropZone.on('dragleave', function(e) {
		let dx = e.pageX - dropZone.offset().left;
		let dy = e.pageY - dropZone.offset().top;
		if ((dx < 0) || (dx > dropZone.width()) || (dy < 0) || (dy > dropZone.height())) {
			dropZone.removeClass('dragover');
		}
	});

	dropZone.on('drop', function(e) {
		dropZone.removeClass('dragover');
		let files = e.originalEvent.dataTransfer.files;
		sendFiles(files);
	});

	$('#file-input').change(function() {
		let files = this.files;
		sendFiles(files);
	});


	function sendFiles(files) 
	{
		let maxFileSize = 5242880;
		let Data = new FormData(dropZone[0]);
		/*$.each( files, function( key, value )
		{
		    alert (JSON.stringify(value));
		    //Data.append( key, value );
		    //Data.append( 'my_file_upload', 1 );
        });*/
        //let Data = new FormData(dropZone[0]);
	    $.each( files, function( key, value ) {
		    //alert (value.name.split('.').pop());
			if ((value.size <= maxFileSize) && (((value.name.split('.').pop()) == 'log') )) 
			{
		      //alert ('Файлы были успешно загружены!');
				
		       Data.append( key, value );
		       Data.append( 'my_file_upload', 1 );
			}
			
		});
		
        if((Data.get('my_file_upload')) === null) 
        {
             alert ("Неверное расширение файла");
             return;
        }
        
		$.ajax({
			//url: dropZone.attr('action'),
		    url: 'upload.php',
			//type: dropZone.attr('method'),
			type: 'POST',
			data: Data,
			contentType: false,
			processData: false,
			success: function(response){
                
               dropZone.submit();
            }
		
			/*success: function(data) {
			    $.each(data, function(key, value){
        $('select[name=category]').append('<option value="' + key + '">' + value +'</option>');
    });
			    //console.log(data['name']);
			    
			    dropZone.submit();
			    	//window.location.href = 'analysis.php';
				//alert ('Файлы были успешно загружены!');
			},
			error: function(data) {
			    console.log(data);
                //alert('Произошла неизвестная ошибка. Попробуйте позже');
            }*/
            
		});
    }
})