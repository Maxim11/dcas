<?php
include "view/InstrumentsStatusView.php";
include "model/InstrumentsStatusModel.php";

class InstrumentsStatusController
{
    public $model;
    public $view;

    public function __construct()
    {
        $this->model = new InstrumentsStatusModel();
        $this->view = new InstrumentsStatusView();
    }

    public function run()
    {
        $devicesStatusData = $this->model->getInstrumentsStatus();
        $this->view->render($devicesStatusData);
    }

    public function getHistory($serialNumber)
    {
        $instrumentHistory = $this->model->getInstrumentHistory($serialNumber);
        $this->view->renderInstrumentHistory($instrumentHistory);
    }
}