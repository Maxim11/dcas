<?php
    include "view/InstrumentsView.php";
    include "view/BaseController.php";
    include "model/InstrumentsModel.php";
        
	class InstrumentsController //extends BaseController
	{	
	    public $model;
	    public $view;
	    
		public function __construct()
		{
		    $this->model = new InstrumentsModel();                        
			$this->view = new InstrumentsView();
			
		}									
                        
		public function run($id, $table_name)		
		{
		    $devices = $this->model->getInstruments($id, $table_name);
		    $this->view->render($devices);
		}

 	}