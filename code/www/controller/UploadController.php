<?php
    include "view/BaseController.php";
    include "view/UploadView.php";
    include "model/UploadModel.php";
        
	class UploadController //extends BaseController
	{		
	    public $model;
	    public $view;
	    
		public function __construct()
		{
		    $this->model = new UploadModel();
		    $this->view = new UploadView();
		}									
                        
		public function run()		
		{
			$this->view->render();		
		}
		
		public function upload()
		{
		    $this->model->uploadAnalisisFile();
		}

 	}