<?php
    include "view/SearchByErrorStartView.php";
    include "view/SearchByErrorDataView.php";
    include "view/BaseController.php";
    include "model/SearchByErrorModel.php";
        
	class SearchByErrorController //extends BaseController
	{	
	    public $model;
	    public $viewStart;
	    public $viewData;
	    
		public function __construct()
		{
		    $this->model = new SearchByErrorModel();                        
			$this->viewStart = new SearchByErrorStartView();
			$this->viewData = new SearchByErrorDataView();
			
		}									
                        
		public function run($id, $table_name)		
		{
		    /*echo 123;
		    var_dump($deviceData);*/
		    $deviceData = $this->model->getDevicesData($id, $table_name);
		    if($deviceData)
		    {
		        $this->viewData->render($deviceData);
		    }
            //echo 1; exit;
            else
            {
			    $this->viewStart->render();		
            }
		}

 	}