<?php

include "view/AnalysisView.php";
include "view/BaseController.php";
include "model/AnalysisModel.php";
        
class AnalysisController //extends BaseController
{			
	public $model;
	public $view;
		
	public function __construct()
	{
		$this->model = new AnalysisModel();                        
		$this->view = new AnalysisView();
	}									
                        
	public function run($device_typeId, $deviceId, $fileName)
	{
		$rulesData = $this->model->getRules($device_typeId, $deviceId);
		//var_dump($rulesData);
		$analysisResult = $this->model->analysisRun($fileName, $rulesData);
		//var_dump($analysisResult);exit;
		$deviceData = $this->model->getDevice($deviceId);
		
        $this->view->render($deviceData, $analysisResult);
	}
}
 	