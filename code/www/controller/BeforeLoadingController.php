<?php

include "view/BeforeLoadingView.php";
include "view/BaseController.php";
include "model/BeforeLoadingModel.php";
        
class BeforeLoadingController //extends BaseController
{			
	public $model;
	public $view;
		
	public function __construct()
	{
		$this->model = new BeforeLoadingModel();                        
		$this->view = new BeforeLoadingView();
	}									
                        
	public function run($id, $table_name)		
	{
		$deviceData = $this->model->getDevicesData($id, $table_name);
	    //echo 'Ok';
		    
        $this->view->render($deviceData);
	}
}
 	