<?php
    include "view/ErrorDescriptionView.php";
    include "view/BaseController.php";
    include "model/ErrorDescriptionModel.php";
        
	class ErrorDescriptionController //extends BaseController
	{	
	    public $model;
	    public $view;
	    
		public function __construct()
		{
		    $this->model = new ErrorDescriptionModel();                        
			$this->view = new ErrorDescriptionView();
		}									
                        
		public function run($rulesId, $devicesId)		
		{
		    $ruleData = $this->model->getRuleData($rulesId, $devicesId);
            //echo 1; exit;
			$this->view->render($ruleData);		
		}

 	}