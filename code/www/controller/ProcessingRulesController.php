<?php
    include "view/BaseController.php";
    include "view/ProcessingRulesView.php";
    include "model/InstrumentTypeModel.php";
    include "model/ProcessingRulesModel.php";
    include "view/InstrumentTypeView.php";
    include "model/RuleTypeModel.php";
    include "view/RuleTypeView.php";
    
        
	class ProcessingRulesController //extends BaseController
	{	
	    public $model;
	    public $view;
	    public $modelInstrumenType;
	    public $viewlInstrumenType;
	    public $modelRuleType;
	    public $viewlRuleType;
	    public $viewUpdate;
	    
		public function __construct()
		{
		    $this->model = new ProcessingRulesModel();     
		    $this->modelInstrumenType = new InstrumentTypeModel();
		    $this->modelRuleType = new RuleTypeModel();
		    
			$this->view = new ProcessingRulesView();
			$this->viewlInstrumenType = new InstrumentTypeView();
			$this->viewlRuleType = new RuleTypeView();
		}									
                        
		public function run($id, $table_name)		
		{
		    $rules = $this->model->getRules($id, $table_name);
		    //var_dump($rules);
		    $instrumentsTypesData = $this->modelInstrumenType->getInstrumentType($id);
		    $instrumentsTypes = $this->viewlInstrumenType->render($instrumentsTypesData); 
		    $rulesTypesData = $this->modelRuleType->getRuleType($id);
		    $rulesTypes = $this->viewlRuleType->render($rulesTypesData);
		    $this->view->render($rules, $instrumentsTypes, $rulesTypes);
		    
		    /*$deviceData = $this->model->getDevicesData($id, $table_name);
		    if($deviceData)
		    {
		        $this->viewData->render($deviceData);
		    }
            //echo 1; exit;
            else
            {
			    $this->viewStart->render($deviceData);		
            }*/
		}
		
		public function createRule()
		{
		    //echo $_FILES['fileClient']['tmp_name'];
		    $fileNames = $this->model->upload();
		    $lastRuleID = $this->model->createRule();
		    
		     //$rules = $this->model->getRules($id, $table_name);
		    
		    //$this->view->render($rules);
		    return 0;
		}
		
		public function updateForm($ruleId)
		{
		    $rule = $this->model->getRules($ruleId);
		    $instrumentsTypesData = $this->modelInstrumenType->getInstrumentType($id);
		    $instrumentsTypes = $this->viewlInstrumenType->render($instrumentsTypesData); 
		    $rulesTypesData = $this->modelRuleType->getRuleType($id);
		    $rulesTypes = $this->viewlRuleType->render($rulesTypesData);
		    $this->view->renderUpdateForm($rule, $instrumentsTypes, $rulesTypes);
		    /*$fileNames = $this->model->upload();
		    $lastRuleID = $this->model->updateRule();*/
		    exit;
		    //return 0;
		}
		
		public function updateRule($ruleId)
		{
		    $this->model->upload();
		    $this->model->updateRule($ruleId);
		    
		    return 0;
		}

        public function deleteRule($ruleId, array $files)
        {
            $this->model->deleteFiles($files);
		    $this->model->deleteRule($ruleId);
		    
            return 0;
        }
	 
        public function readFile($fileName)
        {
            $this->view->readFile($fileName);
            
            return 0;
        }
 	}