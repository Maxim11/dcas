<?php

require_once('BaseView.php');  
//include "BaseView.php";

class InstrumentTypeView extends BaseView
{
    public $instrumentsTypes = '';
    
	public function __construct()
	{
        parent::__construct();
	}
						
	public function render($instrumentsTypes)
	{
        foreach ($instrumentsTypes as &$instrumentType)
		{
			$this->instrumentsTypes .= $this->getPage(['{!TypeId!}', '{!TypeName!}'],
							[$instrumentType->id, $instrumentType->type_name],
							$this->getContent('view/list_item'));		
		}
		
		return $this->instrumentsTypes;
		/*$content = $this->getPage(['{!InstrumentsTypes!}'], [$this->$instrumentsTypes], $this->getContent('view/processingRule'));
	
		echo $this->getPage(['{!contentArea!}', '{!footerContent!}'], 
        [$content, $this->getFooter()],
        $this->getLayout());*/
	}
}	