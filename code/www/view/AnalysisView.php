<?php
include "BaseView.php";

class AnalysisView extends BaseView
{

    public $vertion = [];
    public $values = [];
    public $description = '';
    public $serialNumber = '';
    public $action = '';
    public $helpFile = '';
    
	public function __construct()
	{
        parent::__construct();
	}
						
	public function render($deviceData, $rulesData)
	{
	    $contentError = '';
        $contentVersion = '';
        $contentValue = '';

	    foreach($rulesData as &$ruleData)
	    {
	        $action = $ruleData->user_action;//$rule->action_for_engineer
	        $helpFile = $ruleData->client_file;//$ruleData->engineer_file
	        
	        if($ruleData->rules_type == "error")
            {
                if($contentError == '')
                {
                    $contentError .= $this->getPage(['{!HEADING!}', '{!DESCRIPTION!}', '{!ACTION!}', '{!HELP_FILE!}'],
                        ['Критические ошибки', $ruleData->description,  $action, $helpFile],
                        $this->getContent('view/table_item'));
                }
                else
                {
                    $contentError .= $this->getPage(['{!HEADING!}','{!DESCRIPTION!}', '{!ACTION!}', '{!HELP_FILE!}'],
                        ['', $ruleData->description,  $action, $helpFile],
                        $this->getContent('view/table_item'));
                }

            }

            if($ruleData->rules_type == "version")
            {
                if($contentVersion == '')
                {
                    $contentVersion .= $this->getPage(['{!HEADING!}', '{!DESCRIPTION!}', '{!ACTION!}', '{!HELP_FILE!}'],
                        ['Актуальность версий ПО', $ruleData->description,  $action, $helpFile],
                        $this->getContent('view/table_item'));
                }
                else
                {
                    $contentVersion .= $this->getPage(['{!HEADING!}', '{!DESCRIPTION!}', '{!ACTION!}', '{!HELP_FILE!}'],
                        ['', $ruleData->description,  $action, $helpFile],
                        $this->getContent('view/table_item'));
                }

            }

            if($ruleData->rules_type == "value")
            {
                if($contentValue == '')
                {
                    $contentValue .= $this->getPage(['{!HEADING!}', '{!DESCRIPTION!}', '{!ACTION!}', '{!HELP_FILE!}'],
                        ['Соответствие значений', $ruleData->description,  $action, $helpFile],
                        $this->getContent('view/table_item'));
                }

                else
                {
                    $contentValue .= $this->getPage(['{!HEADING!}', '{!DESCRIPTION!}', '{!ACTION!}', '{!HELP_FILE!}'],
                        ['', $ruleData->description,  $action, $helpFile],
                        $this->getContent('view/table_item'));
                }
            }

	    }

	    if($contentError == '')
        {
            $contentError = $this->getPage(['{!HEADING!}', '{!DESCRIPTION!}'],
                ['Критические ошибки', 'Ошибок не обнаружено'],
                $this->getContent('view/table_item_without_remarks'));
        }

	    if($contentVersion == '')
        {
            $contentVersion = $this->getPage(['{!HEADING!}', '{!DESCRIPTION!}'],
                ['Актуальность версий ПО', "Версии ПО актуальны"],
                $this->getContent('view/table_item_without_remarks'));
        }

	    if($contentValue == '')
        {
            $contentValue = $this->getPage(['{!HEADING!}', '{!DESCRIPTION!}'],
                ['Соответствие значений', "Отклонения не обнаружены"],
                $this->getContent('view/table_item_without_remarks'));
        }

        $content = $this->getPage(['{!SERIAL_NUMBER!}','{!VERTION!}', '{!DESCRIPTION_ERROR!}', '{!VALUE!}'], [$deviceData[0]->serial_number, $contentVersion, $contentError, $contentValue], $this->getContent('view/analysisDescription'));
	    
        echo $this->getPage(['{!contentArea!}', '{!footerContent!}'], 
        [$content, $this->getFooter()],
        $this->getLayout());
	}
}	