<?php
    include "BaseView.php";
	                           	
    class UploadView extends BaseView
    {
	public function __construct()
	{
            parent::__construct();
	}
						
	public function render()
	{
	    //[$this->getContent('view/upload'), $this->getFooter()],
	        $upload = str_replace(['{!instrument type!}', '{!serial_number!}'], [$_POST['device_type'], $_POST['devices']], $this->getContent('view/upload'));
            echo $this->getPage(['{!contentArea!}', '{!footerContent!}'], 
            [$upload, $this->getFooter()],
            //[$this->getContent('view/upload'), $this->getFooter()],
            $this->getLayout());
	}
    }	