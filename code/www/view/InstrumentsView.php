<?php
include "BaseView.php";

class InstrumentsView extends BaseView
{
	public function __construct()
	{
        parent::__construct();
	}
						
	public function render($devices)
	{
	    if($devices)
	    {
	        $data = [];
	        
	       foreach($devices as $device)
	       {
               $data[] = $device;
	       }
	      
            echo json_encode($data);
	    }
	    
	    else
	    {
	        $content = $this->getContent('view/instruments_users');
	    
            echo $this->getPage(['{!contentArea!}', '{!footerContent!}'], 
            [$content, $this->getFooter()],
            $this->getLayout());
	    }
	}
}	