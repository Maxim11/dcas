<?php

require_once('BaseView.php');  
//include "BaseView.php";

class RuleTypeView extends BaseView
{
    public $rulesTypes = '';
    
	public function __construct()
	{
        parent::__construct();
	}
						
	public function render($rulesTypes)
	{
        foreach ($rulesTypes as &$ruleType)
		{
			$this->rulesTypes .= $this->getPage(['{!TypeId!}', '{!TypeName!}'],
							[$ruleType->id, $ruleType->rules_type],
							$this->getContent('view/list_item'));		
		}
	
		return $this->rulesTypes;
		/*$content = $this->getPage(['{!InstrumentsTypes!}'], [$this->$instrumentsTypes], $this->getContent('view/processingRule'));
	
		echo $this->getPage(['{!contentArea!}', '{!footerContent!}'], 
        [$content, $this->getFooter()],
        $this->getLayout());*/
	}
}	