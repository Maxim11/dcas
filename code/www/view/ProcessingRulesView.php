<?php

require_once('BaseView.php');  
//include "BaseView.php";

class ProcessingRulesView extends BaseView
{
    public $rules = '';
    public $rule = '';
    
	public function __construct()
	{
        parent::__construct();
	}
						
	public function render($rules, $instrumentsTypes, $rulesTypes)
	{
	    /*$content = $this->getContent('view/processingRules');
	    
        echo $this->getPage(['{!contentArea!}', '{!footerContent!}'], 
        [$content, $this->getFooter()],
        $this->getLayout());*/
        
        foreach ($rules as &$rule)
		{
			$this->rules .= $this->getPage(['{!RuleId!}', '{!RulesType!}',  '{!DeviceType!}', '{!SerialNumberFirst!}', '{!SerialNumberLast!}', '{!FileType!}',
			'{!MasterKey!}', '{!AdditionalKey!}', '{!Condition!}', '{!ConditionApp!}',  '{!Description!}', 
			'{!User action!}', '{!ActionEngineer!}', '{!EngineerFile!}', '{!ClientFile!}', '{!Alert!}'],
							[$rule->id, $rule->rules_type, $rule->type_name, $rule->serial_number_first,
							$rule->serial_number_last,  $rule->file_type, $rule->master_key, $rule->additional_key, $rule->condition, 
							$rule->conditionApp,  $rule->description,  $rule->user_action,  $rule->action_for_engineer,  
							$rule->engineer_file,  $rule->client_file, $rule->alert],
							$this->getContent('view/containerRule'));		
		}
		
		$content = $this->getPage(['{!rules!}', '{!OptionsTypeInstrument!}', '{!OptionsTypeRule!}'], 
		[$this->rules, $instrumentsTypes, $rulesTypes], $this->getContent('view/processingRule'));
	
		echo $this->getPage(['{!contentArea!}', '{!footerContent!}'], 
        [$content, $this->getFooter()],
        $this->getLayout());
	}
	
	public function renderUpdateForm($rules, $instrumentsTypes, $rulesTypes)
	{
	    foreach($rules as $rule)
	    {
	        $content= $this->getPage(['{!OptionsTypeInstrument!}', '{!OptionsTypeRule!}', '{!RuleId!}', '{!RulesType!}',  '{!DeviceType!}', 
	        '{!SerialNumberFirst!}', '{!SerialNumberLast!}', '{!FileType!}', '{!MasterKey!}', '{!AdditionalKey!}', '{!Condition!}',
	        '{!ConditionApp!}','{!Description!}', '{!User action!}', '{!ActionEngineer!}', '{!EngineerFile!}', '{!ClientFile!}', '{!Alert!}'],
							[$instrumentsTypes, $rulesTypes, $rule->id, $rule->rules_type, $rule->type_name, $rule->serial_number_first, 
							$rule->serial_number_last,  $rule->file_type, $rule->master_key, $rule->additional_key, $rule->condition, 
							$rule->conditionApp, $rule->description,  $rule->user_action,  $rule->action_for_engineer,  $rule->engineer_file,  $rule->client_file, $rule->alert],
							$this->getContent('view/updateRuleForm'));
	    }					
	    
		echo $this->getPage(['{!contentArea!}', '{!footerContent!}'], 
        [$content, $this->getFooter()],
        $this->getLayout());					
	}
	
	public function readFile($fileName)
	{
	    $content = file_get_contents('uploads/'."$fileName");
	    
	    echo $this->getPage(['{!contentArea!}', '{!footerContent!}'], 
        [$content, $this->getFooter()],
        $this->getLayout());
	}
}	