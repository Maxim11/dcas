<?php
include "BaseView.php";

class InstrumentsStatusView extends BaseView
{
    public $historyDevices = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function render($devicesStatusData)
    {
        foreach ($devicesStatusData as &$deviceStatusData)
        {
            $this->historyDevices .= $this->getPage(['{!SerialNumber!}', '{!InstrumentTupe!}', '{!UserData!}', '{!SoftwareVersion!}',
                '{!LastLogDate!}', '{!CriticalErrors!}'],
                [$deviceStatusData->serial_number, $deviceStatusData->instrument_type, $deviceStatusData->user_uploaded_file, $deviceStatusData->software_versions,
                    $deviceStatusData->upload_date,  $deviceStatusData->critical_errors],
                $this->getContent('view/containerInstrumentStatus'));
        }

        $content = $this->getPage(['{!HistoryDevices!}'],
            [$this->historyDevices], $this->getContent('view/instrumentsStatus'));

        echo $this->getPage(['{!contentArea!}', '{!footerContent!}'],
            [$content, $this->getFooter()],
            $this->getLayout());
    }

    public function renderInstrumentHistory($devicesHistoryData)
    {
        foreach ($devicesHistoryData as &$deviceStatusData)
        {
            $this->historyDevices .= $this->getPage(['{!SerialNumber!}', '{!InstrumentTupe!}', '{!UserData!}', '{!SoftwareVersion!}',
                '{!LastLogDate!}', '{!CriticalErrors!}', '{!DataFile!}'],
                [$deviceStatusData->serial_number, $deviceStatusData->instrument_type, $deviceStatusData->user_uploaded_file, $deviceStatusData->software_versions,
                    $deviceStatusData->upload_date,  $deviceStatusData->critical_errors, $deviceStatusData->data_file],
                $this->getContent('view/containerInstrumentHistory'));
        }

        $content = $this->getPage(['{!HistoryDevices!}'],
            [$this->historyDevices], $this->getContent('view/instrumentsStatus'));

        echo $this->getPage(['{!contentArea!}', '{!footerContent!}'],
            [$content, $this->getFooter()],
            $this->getLayout());

        exit;
    }
}	