<?php
include "BaseView.php";
	                           	
class BeforeLoadingView extends BaseView
{
	public function __construct()
    {
        parent::__construct();
    }
						
    public function render($devicesData)
	{
	    if($devicesData)
	    {
	        $data = [];
	        
	       foreach($devicesData as $deviceData)
	       {
               $data[] = $deviceData;
	       }
	      
            echo json_encode($data);
	    }
	    
	    else
	    {
	        echo $this->getPage(['{!contentArea!}', '{!footerContent!}'], 
                [$this->getContent('view/BeforeLoading'), $this->getFooter()],
                $this->getLayout());
	    }
        
	}
}	