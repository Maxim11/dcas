<?php
include "BaseView.php";
class ErrorDescriptionView extends BaseView
{
    public $description = '';
    public $serialNumber = '';
    public $action = '';
    public $helpFile = '';
    
	public function __construct()
	{
        parent::__construct();
	}
						
	public function render($ruleData)
	{
	    foreach($ruleData as &$data)
	    {
	        $this->serialNumber = $data->serial_number;
	        $this->description = $data->description;
	        $this->action = $data->action_for_engineer;
	        $this->helpFile = $data->engineer_file;
	    }
	    
	    $content = $this->getPage(['{!SERIAL_NUMBER!}','{!DESCRIPTION_ERROR!}', '{!ACTION!}', '{!HELP_FILE!}'], [$this->serialNumber, $this->description, $this->action, $this->helpFile], $this->getContent('view/errorDescription'));
	    
        echo $this->getPage(['{!contentArea!}', '{!footerContent!}'], 
        [$content, $this->getFooter()],
        $this->getLayout());
	}
}	